#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<graphics.h>
#include<process.h>

getkey()
{
    union REGS i,o;
    while(!kbhit());
    i.h.ah=0;
    int86(22,&i,&o);
    return (o.h.ah);
}

void welcome()
{
    clrscr();
    gotoxy(30,4);
    printf("Welcome to mini piano.");
    gotoxy(22,8);
    printf("Programmer: Arun(ak007)");
    gotoxy(30,12);
    printf("Project: Mini Piano v1.0");
    gotoxy(25,20);
    printf("Press any key to play piano......");
    gotoxy(32,24);
    printf("Press Esc to exit");
    getch();
}

void layout()
{
    int x=getmaxx()/2,y=getmaxy()/2;
    int bar_wh=250,bar_wb=50,bar_bh=110,bar_bb=45;
    setfillstyle(1,0);
    bar(40,60,60,160);
    int i,x1=x-bar_wb*6,y1=y-bar_wh-50;

    char str[10];
    char mat1[12]={'Q','W','E','R','T','Y','U','I','O','P','[',']'};
    char mat2[12]={'1','2','3','4','5','6','7','8','9','0','-','+'};
   /* char mat3[12]={0,'Z','X','C','V','B','N','M',',','.','/'};
    char mat4[12]={0,0,'S','D','F','G','H','J','K','L',';',0};*/

    setcolor(0);
    settextstyle(2,0,7);
    setfillstyle(1,15);
    for(i=0;i<12;i++)
    {       setcolor(0);
        bar3d(x1+i*bar_wb,y1,x1+(i+1)*bar_wb-2,y1+bar_wh,6,7);
        setcolor(0);
        sprintf(str,"%c",mat1[i]);
        outtextxy(x1+i*bar_wb+17,y1+bar_wh-30,str);
    }
  /*  for(i=1;i<=10;i++)
    {
        bar3d(x1+i*bar_wb,y1+bar_wh+50,x1+(i+1)*bar_wb-2,y1+2*bar_wh+50,6,7);
        sprintf(str,"%c",mat3[i]);
        outtextxy(x1+i*bar_wb+17,y1+2*bar_wh+20,str);
    }*/
    setfillstyle(1,0);
    for(i=1;i<=10;i++)
    {       if(i!=4&&i!=7)
        {
            bar(x1+i*bar_wb-bar_bb/2,y1,x1+i*bar_wb-2+bar_bb/2,y1+bar_bh);
            setcolor(8);
            rectangle(x1+i*bar_wb-bar_bb/2+1,y1,x1+i*bar_wb-2+bar_bb/2-1,y1+bar_bh-1);
            setcolor(15);
            sprintf(str,"%c",mat2[i]);
            outtextxy(x1+i*bar_wb+12-bar_bb/2,y1+bar_bh-30,str);
        }
    }
    /*for(i=2;i<=10;i++)
    {       if(i!=4&&i!=8)
        {    bar(x1+i*bar_wb-bar_bb/2,y1+bar_wh+50,x1+i*bar_wb-2+bar_bb/2,y1+bar_wh+50+bar_bh);
            setcolor(8);
            rectangle(x1+i*bar_wb-bar_bb/2+1,y1+bar_wh+50,x1+i*bar_wb-2+bar_bb/2-1,y1+bar_wh+50+bar_bh-1);
            setcolor(15);
            sprintf(str,"%c",mat4[i]);
            outtextxy(x1+i*bar_wb+12-bar_bb/2,y1+bar_wh+bar_bh+20,str);
        }
    }*/
}
int piano()
{
    int gd=DETECT,gm;
    initgraph(&gd,&gm,"..\\bgi");
    layout();
    int soun[100]={22,23,24,26,27,29,31,33,35,37,39,41,44,46,49,52,55,58,62,65,69,73,78,82,87,92,98,104,110,116,123,131,139,147,155,165,175,185,196,208,220,233,245,262,277,294,311,330,349,370,392,415,440,466,494,523,554,587,622,659,698,740,784,831,880,932,988,1046,1109,1175,1244,1328,1397,1480,1568,1661,1760,1865,1975,2093,2217,2349,2489,2637,2749,2960,3136,3322,3520,3729,3951,4186};
    int f,ch,octave=4,freq;
    while(1)
    {
        ch=getkey();
                  switch(ch)
        {
            case 1: return 0;

            case 16: f =octave*12+0;    break;
            case 17: f =octave*12+2;    break;
            case 18: f =octave*12+4;    break;
            case 19: f =octave*12+6;    break;
            case 20: f =octave*12+7;    break;
            case 21: f =octave*12+9;    break;
            case 22: f =octave*12+11;    break;
            case 23: f =octave*12+12;    break;
            case 24: f =octave*12+14;    break;
            case 25: f =octave*12+16;    break;
            case 26: f =octave*12+18;    break;
            case 27: f =octave*12+19;    break;

            case 3: f =octave*12+1;        break;
            case 4: f =octave*12+3;        break;
            case 5: f =octave*12+5;        break;
            case 7: f =octave*12+8;        break;
            case 8: f =octave*12+10;    break;
            case 10: f =octave*12+13;    break;
            case 11: f =octave*12+15;    break;
            case 12: f =octave*12+17;    break;


          /*  case 53: f =octave*12-1;    break;
            case 52: f =octave*12-3;    break;
            case 51: f =octave*12-5;    break;
            case 50: f =octave*12-6;    break;
            case 49: f =octave*12-8;    break;
            case 48: f =octave*12-10;    break;
            case 47: f =octave*12-12;    break;
            case 46: f =octave*12-13;    break;
            case 45: f =octave*12-15;    break;
            case 44: f =octave*12-17;    break;

            case 39: f =octave*12-2;    break;
            case 38: f =octave*12-4;    break;
            case 36: f =octave*12-7;    break;
            case 35: f =octave*12-9;    break;
            case 34: f =octave*12-11;    break;
            case 32: f =octave*12-14;    break;
            case 31: f =octave*12-16;    break;*/

            case 77:
            case 72: if(octave<6)    octave++; break;
            case 75:
            case 80: if(octave>1)    octave--; break;

            default:    freq =0;
        }
        if(freq!=0)
        {
            freq =soun[f];
            sound(freq);
            delay(120);
            nosound();
        }
        else
            freq=1;
    }
}
int  main()
{
    welcome();
    piano();
    return 0;
}
